<p align="center"><a href="https://bitbucket.org/eduard-molchanov/task-5/src/master/" target="_blank">
<img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a>
</p>



## Поднятие Laravel в docker



- >Склонируйте  [репозиторий](https://bitbucket.org/eduard-molchanov/task-5/src/master/).
- >Откройте катлог с клоном в терминале.
- >выполните команду **composer update**.
- >переиенуйте файл **.env.example** в **.env**
- >установите в файле **.env** следующие значения:
  
<p align="center">DB_HOST=mysql</p>

<p align="center">DB_DATABASE=task5</p>

<p align="center">DB_PASSWORD=root</p>


- >выполните команду **php artisan key:generate**
- >выполните команду **docker-compose build**
- >выполните команду **docker-compose up -d**
- >выполните коанду **docker ps**
- > должно получиться примерно так:
![screenshot of sample](docker/ps.jpg)

- >в браузере перейдите по адресу [http://localhost:8098/](http://localhost:8098/)
- > должно получиться  так:
  ![screenshot of sample](docker/laravel.jpg)
